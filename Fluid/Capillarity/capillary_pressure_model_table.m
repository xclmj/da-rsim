% Table capillary pressure model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DARSim 2 Reservoir Simulator
%Author: Yuhang Wang
%TU Delft
%Created: 3 Feb 2021
%Last modified: 14 July 2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef  capillary_pressure_model_table < capillary_pressure_model
    properties
        Pc
        dPc
    end
    methods
        function Initialise(obj, ProductionSystem)
            N = length(ProductionSystem.Reservoir.Por);
            % allocate memory
            obj.Pc = zeros(N,1);
            obj.dPc = zeros(N,1); 
        end
%         function ComputePc(obj, RelPermModel, State)
%             S = State.Properties('S_1').Value;
%             status_P_1 = State.Properties('P_1');
%             status_P_2 = State.Properties('P_2');
%             status_Pc = State.Properties('Pc'); 
%             % Compute Pc as a function of S_1
%             
%             % no hysteresis
%             Pc_nohysteresis =  RelPermModel.Pc_drainage_Sg_table(S);
%             Pc_nohysteresis(:) = 0;
%             
%             Pc_nohysteresis(Pc_nohysteresis<0)=0;
%             status_Pc.Value = Pc_nohysteresis;
%             % update pressure;
%             status_P_1.Value = status_P_2.Value + Pc_nohysteresis; 
%         end
%         function dPc = dPcdS(obj, RelPermModel, S)
%             % S = State.Properties('S_1').Value;
%             % no hysteresis
%             dPc = -RelPermModel.dPc_drainage_Sg_table(S);
%             dPc(:) = 0;
%         end
        function ComputePcHysteresis(obj, RelPermModel, DiscretizationModel, State)
            S = State.Properties('S_1').Value;
            status_P_1 = State.Properties('P_1');
            status_P_2 = State.Properties('P_2');
            status_Pc = State.Properties('Pc');
            % Compute Pc as a function of S_1
            
%             % no hysteresis
%             obj.Pc =  RelPermModel.Pc_drainage_Sg_table(S);
%             obj.dPc = -RelPermModel.dPc_drainage_Sg_table(S);

            % with hysteresis
            for i = 1:length(S)
                if (RelPermModel.process_label(i)==0)
                    obj.Pc(i) =  RelPermModel.Pc_drainage_Sg_table(S(i));
                    obj.dPc(i) = -RelPermModel.dPc_drainage_Sg_table(S(i)); 
                else
                   scanning_curve_Pc_table = griddedInterpolant(RelPermModel.scanning_curve_Sg(i,:),RelPermModel.scanning_curve_Pc(i,:));
                   scanning_curve_dPc_table = griddedInterpolant(RelPermModel.scanning_curve_Sg(i,:),RelPermModel.scanning_curve_dPc(i,:)); 
                   obj.Pc(i) = scanning_curve_Pc_table(S(i));
                   obj.dPc(i) = -scanning_curve_dPc_table(S(i));
                end 
            end
            
            obj.Pc(obj.Pc<0)=0;
            % convergence consideration
            obj.Pc(DiscretizationModel.ReservoirGrid.ListOfFracturedReservoirCells) = obj.Pc(DiscretizationModel.ReservoirGrid.ListOfFracturedReservoirCells)*0.01;
            obj.dPc(DiscretizationModel.ReservoirGrid.ListOfFracturedReservoirCells) = obj.dPc(DiscretizationModel.ReservoirGrid.ListOfFracturedReservoirCells)*0.01;
            
            status_Pc.Value = obj.Pc;
            % update pressure;
            status_P_1.Value = status_P_2.Value + obj.Pc;
        end
        function ComputePcnoHysteresis(obj, RelPermModel, State)
            S = State.Properties('S_1').Value;
            status_P_1 = State.Properties('P_1');
            status_P_2 = State.Properties('P_2');
            status_Pc = State.Properties('Pc');
            % Compute Pc as a function of S_1
            
            % no hysteresis
            Pc_value =  RelPermModel.Pc_drainage_Sg_table(S);
            % dPc = -RelPermModel.dPc_drainage_Sg_table(S);

            Pc_value(Pc_value<0)=0;
            status_Pc.Value = Pc_value;
            % update pressure;
            status_P_1.Value = status_P_2.Value + Pc_value;
        end        
        function dPc = dPcdSHysteresis(obj)
            dPc = obj.dPc;
        end
        function dPc = dPcdSnoHysteresis(obj, RelPermModel, State)
            S = State.Properties('S_1').Value; 
            % no hysteresis
            dPc = -RelPermModel.dPc_drainage_Sg_table(S);
        end 
        function d2Pc = dPcdSdS(obj, S)
        end
        
    end
    
end